# Collatz conjecture calculator

This is a program that is able to calculate what numbers the Collatz conjecture
ends up repeating for a given seed between 1 and 2¹²⁸. This does make it mostly
useless since we already know all of those end up repeating the sequence
1, 2, 4, but it was fun to write.
