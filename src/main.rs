// SPDX-License-Identifier: ISC
use std::{collections::VecDeque, io, io::Write};

const DEQUE_SIZE: usize = 6;

fn main() -> Result<(), io::Error> {
    let mut numbers = VecDeque::with_capacity(DEQUE_SIZE);
    // - 1 to leave space for element pushed back later
    numbers.resize(DEQUE_SIZE - 1, 0);

    print!("Enter a number: ");
    io::stdout().flush()?;

    let mut line = String::new();
    io::stdin().read_line(&mut line)?;
    let input = line.trim_end();

    let input_int: u128 = input.parse().unwrap();

    numbers.push_back(input_int);
    let mut iterations = 0;

    loop {
        iterations += 1;
        let back = *numbers.back().unwrap();
        numbers.pop_front();

        let new_back;

        if back % 2 == 0 {
            new_back = back / 2;
        } else {
            new_back = back * 3 + 1;
        }

        numbers.push_back(new_back);

        if numbers.get(0) == numbers.get(3)
            && numbers.get(1) == numbers.get(4)
            && numbers.get(2) == numbers.get(5) {
            break;
        }
    }

    println!("{:#?}", numbers);
    println!("Iterations: {}", iterations);

    Ok(())
}
